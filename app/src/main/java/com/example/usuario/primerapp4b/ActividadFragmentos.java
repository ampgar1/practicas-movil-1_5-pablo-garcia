package com.example.usuario.primerapp4b;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadFragmentos extends AppCompatActivity implements View.OnClickListener, FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener {
    Button botonFragUno, botonFragDos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_fragmentos);

        botonFragUno = (Button) findViewById(R.id.btnFragmento1);
        botonFragDos = (Button) findViewById(R.id.btnFragmento2);

        botonFragUno.setOnClickListener(this);
        botonFragDos.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFragmento1:
        FragUno  fragmentoUno = new FragUno();
            FragmentTransaction transactionUno = getSupportFragmentManager(). beginTransaction();
            transactionUno.replace(R.id.contenedor, fragmentoUno);
            transactionUno.commit();
            break;
            case  R.id.btnFragmento2:
          FragDos fragmentoDos = new FragDos();
          FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
          transactionDos.replace(R.id.contenedor, fragmentoDos);
          transactionDos.commit();
          break;
        }
    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    }